import time
from pykube import Deployment, Pod


def setup_args(api, args):
    deploy = Deployment.objects(api).get(name="testapp")
    deploy.obj["spec"]["template"]["spec"]["containers"][0]["args"] = args
    deploy.update()

    while True:
        deploy = Deployment.objects(api).get(name="testapp")
        pods = list(Pod.objects(api).filter(selector="application=testapp"))
        if (
            len(pods) == 1
            and deploy.obj["status"]["updatedReplicas"] == 1
            and deploy.obj["status"]["readyReplicas"] == 1
        ):
            break
        time.sleep(0.5)


def do_rolling_update(api, capsys):
    start = time.time()

    deploy = Deployment.objects(api).get(name="testapp")
    deploy.obj["spec"]["template"]["metadata"]["labels"]["version"] = f"{start}"
    deploy.update()

    while True:
        deploy = Deployment.objects(api).get(name="testapp")
        pods = list(Pod.objects(api).filter(selector="application=testapp"))
        if (
            len(pods) == 1
            and pods[0].metadata["labels"]["version"] == f"{start}"
            and deploy.obj["status"]["updatedReplicas"] == 1
            and deploy.obj["status"]["readyReplicas"] == 1
        ):
            break
        time.sleep(0.5)

    # wait another 5 seconds for things to settle
    time.sleep(5)

    elapsed = 0

    logs = []
    for pod in Pod.objects(api).filter(selector="application=probe"):
        for line in pod.logs().split("\n"):
            if line:
                parts = line.split(":")
                assert parts[1] == "root"
                ts = float(parts[2])
                if ts > start:
                    logs.append(line)
                    elapsed = ts - start

    error_count = len(list(line for line in logs if "ERROR" in line))
    success_count = len(list(line for line in logs if "STATUS:200" in line))

    with capsys.disabled():
        print(f"Elapsed:    {elapsed:.2f} seconds")
        print(f"Successes:  {success_count}")
        print(f"Errors:     {error_count}")
        print(f"Requests/s: {(success_count + error_count)/elapsed: .2f} rps")

    errors = list(line for line in logs if "ERROR" in line)
    return elapsed, success_count, errors


def test_rolling_update_no_signal_handling(populated_cluster, capsys):
    setup_args(populated_cluster.api, ["testapp"])
    elapsed, success_count, errors = do_rolling_update(populated_cluster.api, capsys)
    assert elapsed > 30
    assert errors == []


def test_rolling_update_with_signal_handling(populated_cluster, capsys):
    setup_args(populated_cluster.api, ["testapp", "--handle-signals"])
    elapsed, success_count, errors = do_rolling_update(populated_cluster.api, capsys)
    assert elapsed > 0
    assert errors != []


def test_rolling_update_sleep_on_shutdown(populated_cluster, capsys):
    setup_args(populated_cluster.api, ["testapp", "--sleep-on-shutdown"])
    elapsed, success_count, errors = do_rolling_update(populated_cluster.api, capsys)
    assert elapsed > 0
    assert errors != []
