.PHONY: clean test appjs docker push mock

IMAGE            ?= aiohttp-kubernetes
GITDIFFHASH       = $(shell git diff | md5sum | cut -c 1-4)
VERSION          ?= $(shell git describe --tags --always --dirty=-dirty-$(GITDIFFHASH))
TAG              ?= $(VERSION)
TTYFLAGS          = $(shell test -t 0 && echo "-it")
OSNAME := $(shell uname | perl -ne 'print lc($$_)')

default: docker

.PHONY: poetry
poetry:
	poetry install

.PHONY: test
test: poetry lint test.e2e

.PHONY: lint
lint: 
	flake8
	poetry run black --check *.py

.PHONY: test.e2e
test.e2e: kind kubectl docker
	env TEST_IMAGE=$(IMAGE):$(TAG) \
		poetry run pytest -v -r=a \
			--log-cli-level info \
			--log-cli-format '%(asctime)s %(levelname)s %(message)s' \
			--cluster-name aiohttp-kubernetes \
			tests/e2e $(PYTEST_OPTIONS)

docker: 
	docker build --build-arg "VERSION=$(VERSION)" -t "$(IMAGE):$(TAG)" .
	@echo 'Docker image $(IMAGE):$(TAG) can now be used.'
