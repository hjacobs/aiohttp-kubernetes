aiohttp on Kubernetes
=====================

This repository shows how to configure aiohttp on Kubernetes to allow rolling updates without failing requests.

There are a number of approaches which **do not work**:

* default `web.run_app` will handle `SIGTERM` and leads to failing requests during rolling pod updates
* using aiohttp's `on_shutdown` signal to sleep 20 seconds will not work as aiohttp will already close the server before trigering `on_shutdown`


