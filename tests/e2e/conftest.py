import yaml
from tempfile import NamedTemporaryFile
import logging
from pytest import fixture
from pathlib import Path
import os


@fixture(scope="session")
def populated_cluster(kind_cluster):

    docker_image = os.getenv("TEST_IMAGE")
    kind_cluster.load_docker_image(docker_image)

    logging.info("Deploying testapp...")

    for fn in ("deployment", "probe"):
        deployment_manifests_path = Path(__file__).parent / f"{fn}.yaml"

        with NamedTemporaryFile(mode="w+") as tmp:
            with deployment_manifests_path.open() as f:
                resources = list(yaml.safe_load_all(f))
            dep = resources[-1]
            assert dep["kind"] == "Deployment"
            dep["spec"]["template"]["spec"]["containers"][0]["image"] = docker_image
            yaml.dump_all(documents=resources, stream=tmp)
            kind_cluster.kubectl("apply", "-f", tmp.name)

    logging.info("Waiting for rollout ...")
    kind_cluster.kubectl("rollout", "status", "deployment/testapp")
    kind_cluster.kubectl("rollout", "status", "deployment/probe")

    return kind_cluster
