FROM python:3.7-alpine

WORKDIR /

RUN pip3 install aiohttp requests

COPY web.py /

ENTRYPOINT ["/usr/local/bin/python", "/web.py"]
