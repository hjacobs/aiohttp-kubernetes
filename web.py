#!/usr/bin/env python3
"""
Example implementation of REST endpoint for a Cluster Registry.

To be used with --cluster-registry-url option
"""

import argparse
import asyncio
import logging
import requests
import time
from aiohttp import web


logging.basicConfig(level=logging.INFO)

routes = web.RouteTableDef()


@routes.get("/")
async def get_index(request):
    await asyncio.sleep(0.01)
    return web.json_response({"message": "Hello world!"})


@routes.get("/health")
async def get_health(request):
    return web.Response(status=200, text="OK")


async def on_shutdown(app):
    await asyncio.sleep(20)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("command")
    parser.add_argument("--probe-url")
    parser.add_argument("--handle-signals", action="store_true", default=False)
    parser.add_argument("--sleep-on-shutdown", action="store_true", default=False)
    args = parser.parse_args()

    if args.command == "probe":
        while True:
            try:
                response = requests.get(args.probe_url, timeout=1)
                logging.info(f"{time.time()}:STATUS:{response.status_code}")
            except Exception as e:
                logging.error(f"{time.time()}:ERROR:{e}")

    elif args.command == "testapp":
        app = web.Application()
        app["shutdown"] = False
        app.add_routes(routes)
        if args.sleep_on_shutdown:
            app.on_shutdown.append(on_shutdown)
        web.run_app(app, port=8080, handle_signals=args.handle_signals)

    else:
        raise Exception("Invalid command arg!")
